import java.util.Scanner;


public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
	  Scanner scanner = new Scanner (System.in);
	   
	  System.out.println("Geben Sie eine Zahl ein:");
      double x = scanner.nextDouble();
      System.out.println("Geben Sie eine Zahl ein:");
      double y = scanner.nextDouble();
      double m;
      
      System.out.println("Wie viele Zahlen wollen Sie eingeben: ");
      int anzahl = scanner.nextInt();
      int zaehler = 0;
      double summe = 0;
      
      while(zaehler < anzahl) {
    	  System.out.println("Bitte geben Sie eine Zahl ein: ");
    	  double z = scanner.nextDouble();
    	  summe = summe + z;
    	  zaehler++;
      }
      
      
      
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
     m = mittelwert (x,y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      ausgabe(x,y,m);
      
   }
   public static double mittelwert(double  x, double y) {
	   return (x + y) / 2.0;
   }
   
   public static void ausgabe(double x, double y, double m) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
}