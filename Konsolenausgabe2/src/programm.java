
public class programm {

	public static void main(String[] args) {
		
		
		//Konsolenausgabe_2_Aufgabe 1:
			System.out.println("   **   ");
			System.out.println("*      *");
			System.out.println("*      *");
			System.out.println("   **   ");
			
		//Konsolenausgabe_2_Aufgabe 2:
			System.out.printf("%-5s = %-18s = %4d\n", "0!", "", 1);
			System.out.printf("%-5s = %-18s = %4d\n", "1!", "1", 1);
			System.out.printf("%-5s = %-18s = %4d\n", "2!", "1 * 2", 1 * 2);
			System.out.printf("%-5s = %-18s = %4d\n", "3!", "1* 2 * 3", 1 * 2 * 3);
			System.out.printf("%-5s = %-18s = %4d\n", "4!", "1 * 2 * 3 * 4", 1 * 2 * 3 * 4);
			System.out.printf("%-5s = %-18s = %4d\n", "5!", "1 * 2 * 3 * 4 * 5", 1 * 2 * 3 * 4 * 5);
		
		
		//Konsolenausgabe_1_Aufgabe 3:
			System.out.printf("%.2f\n", 22.4234234);
			System.out.printf("%.2f\n", 111.2222);
			System.out.printf("%.2f\n", 4.0);
			System.out.printf("%.2f\n", 1000000.551);
			System.out.printf("%.2f\n", 97.34);
		
		//Konsolenausgabe_2_Aufgabe 3:
		
			System.out.print("Fahrenheit | Celsius");
			System.out.println("\n-----------------------");
			System.out.printf("%-10s | %10.2f\n", "-20", -28.8889);
			System.out.printf("%-10s | %10.2f\n", "-10", -23.3333);
			System.out.printf("%-10s | %10.2f\n", "+0",-17.7778);
			System.out.printf("%-10s | %10.2f\n", "+20", -6.6667);
		    System.out.printf("%-10s | %10.2f\n" , "+30", -1.1111);
	
		    
		    //final update
		    //Jetzt sollte alles funktionieren!!
	}
}
