
public class Program {

	public static void main(String[] args) {
		System.out.println("Die \"Aufgabe\" ist fertig.\nDas ist eine Aufgabe.");
		System.out.println("____________*");
		System.out.println("___________***");
		System.out.println("__________*****");
		System.out.println("_________*******");
		System.out.println("________*********");
		System.out.println("_______***********");
		System.out.println("______*************");
		System.out.println("___________***");
		System.out.println("___________***");
	}
// Unterschied zwischen print(Printed normal in die anderen Zeilen mit dazu) und println(Printed alle in eine einzelne Zeile)
}
