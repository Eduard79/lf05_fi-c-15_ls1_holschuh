import java.util.Scanner;

public class fahrkartenautomat2 {
	

	
	    public static void main(String[] args)
	    {
	       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	       double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	       fahrkartenAusgeben();
	       rueckgeldAusgeben(r�ckgabebetrag);

	    }
	    
	    	
	    
	    // Fahrkarten-Bestellung-Erfassen
	    
	    public static double fahrkartenbestellungErfassen() 
	    {
	 	   Scanner tastatur = new Scanner(System.in);
		   
	 	   double zuZahlenderBetrag;
	 	   short anzahlTickets;
	 	   double zuZahlenderGesamtbetrag;
	 	   
	        System.out.print("Zu zahlender Betrag (EURO): ");
	        zuZahlenderBetrag = tastatur.nextDouble();
	        
	        // Anzahl Tickets * zuZahlenderBetrag
	        
	        System.out.println("Anzahl der Tickets: ");
	        anzahlTickets = tastatur.nextShort();
	        
	        if (anzahlTickets > 10 || anzahlTickets < 1){
	        System.out.print("Die Anzahl der Tickets wird auf 1 gesetzt!");
	        anzahlTickets= 1;
	        }
	        
	        zuZahlenderGesamtbetrag = zuZahlenderBetrag * anzahlTickets;
	        return zuZahlenderGesamtbetrag;
	    }

	    	//Fahrkarten-Bezahlung
	    
		public static double fahrkartenBezahlen(double zuZahlenderGesamtbetrag) 
		{
		    Scanner tastatur = new Scanner(System.in);
			double eingezahlterGesamtbetrag;
		    double eingeworfeneM�nze;
		    double r�ckgabebetrag;
		    
			// Geldeinwurf
		    // -----------
		    eingezahlterGesamtbetrag = 0.00;
		    while(eingezahlterGesamtbetrag < zuZahlenderGesamtbetrag)
		    {
		    	System.out.printf("Noch zu zahlen: " +  "%.2f", (zuZahlenderGesamtbetrag - eingezahlterGesamtbetrag));
		    	System.out.println(" Euro");
		    	System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		    	eingeworfeneM�nze = tastatur.nextDouble();
		        eingezahlterGesamtbetrag += eingeworfeneM�nze;
		    }
		    r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderGesamtbetrag;
		    tastatur.close();
		    return r�ckgabebetrag;
		}
		
		
		// Fahrkarten-Ausgabe
		
		public static void fahrkartenAusgeben() 
		{
		       System.out.println("\nFahrschein wird ausgegeben");
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("====");
		          try {
					Thread.sleep(250);
		          	} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
		       System.out.println("\n\n");
		       
		}
		
		
		// R�ckgeldberechnung und -Ausgabe
	      // -------------------------------
		public static void rueckgeldAusgeben(double r�ckgabebetrag) 
		{
		       
		       if(r�ckgabebetrag > 0.0)
		       {
		    	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
		    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

		           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nze
		           {
		        	  System.out.println("2 EURO");
			          r�ckgabebetrag -= 2.0;
		           }
		           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nze
		           {
		        	  System.out.println("1 EURO");
			          r�ckgabebetrag -= 1.0;
		           }
		           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nze
		           {
		        	  System.out.println("50 CENT");
			          r�ckgabebetrag -= 0.5;
		           }
		           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nze
		           {
		        	  System.out.println("20 CENT");
		 	          r�ckgabebetrag -= 0.2;
		           }
		           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nze
		           {
		        	  System.out.println("10 CENT");
			          r�ckgabebetrag -= 0.1;
		           }
		           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nze
		           {
		        	  System.out.println("5 CENT");
		 	          r�ckgabebetrag -= 0.05;
		 	          
		           }
		       }
		       System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
		    		   			"vor Fahrtantritt entwerten zu lassen!\n" +
		    		   			"Tragen Sie eine FFP2-Maske und halten Sie Ihren 3G-Nachweis bereit!");
		       
			   	
		}
	      

		
	       	/* Erkl�rungen: 
	       	* ------------------------------------------------------------------------------------------------------------------
	        * -> "zuZahlenderBetrag" * "anzahlTickets"
	        * -> while "eingezahlterGesamtbetrag" < "eingezahlterGesamtbetrag" wird dieser vom "zuzahlendenBetrag" subtrahiert
	        * -> "eingezahlterBetrag" wird mit "eingezahlterGesamtbetrag" addiert
	        * -> "eingezahlterGesamtbetrag" - "zuzahlenderGesamtbetrag"
	        * -> der "R�ckgabebetrag" wird anschlie�end vom Automaten ausgezahlt! 
	        * ------------------------------------------------------------------------------------------------------------------*/
	
}
