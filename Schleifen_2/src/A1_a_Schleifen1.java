import java.util.Scanner;

public class A1_a_Schleifen1 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Maximalwert eingeben:");
		int maxValue = scanner.nextInt();
		
		for (int i = 0; i <= maxValue; i++) {
			
			System.out.println(i);
		}

	}

}
