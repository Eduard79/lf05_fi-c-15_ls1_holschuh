
public class Volumenberechnung {

	public static void main(String[] args) {
		
		System.out.println("Vor dem Methodenaufruf:");
		System.out.println("______________________");
		volumenberechnung();
		System.out.println("______________________");
		System.out.println("Nach dem Methodenaufruf");

	}
	public static void volumenberechnung() {
		int a = 2;
		int b = 2;
		int c = 2;
		int h = 2;
		int r = 2;
		int wuerfel_volumen = a * a * a;
		int quader_volumen =  a * b * c;
		int pyramide_volumen = a * a * h/3;
		int kugel_volumen = (int) (4/3 * r * 3.1415926353);
		
		System.out.println("Das Volumen eines W�rfels mit a:2 betr�gt = " + wuerfel_volumen);
		System.out.println("Das Volumen eines Quaders mit a:2, b:2 und c:2 betr�gt = " + quader_volumen);
		System.out.println("Das Volumen einer Pyramide mit a:2 und h:2 betr�gt = " + pyramide_volumen);
		System.out.println("Das Volumen einer Kugel mit r:2 betr�gt = " + kugel_volumen);
	}

}
